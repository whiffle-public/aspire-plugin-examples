import pandas as pd
import numpy as np
import xarray as xr

ds = xr.Dataset()

ds = ds.assign_coords(time=pd.date_range("2020/01/01", freq="1h", periods=20))

ds = ds.assign(
    {"some_variable": ("time", np.arange(ds.time.size))}
)

ds.to_netcdf("some_dataset.nc")
