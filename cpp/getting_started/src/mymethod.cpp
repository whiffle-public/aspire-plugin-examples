#include "mymethod.h"

#include <memory>
#include <iostream>

#include <xtensor/xarray.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xview.hpp>

#include <dataset.h>
#include <ncreader.h>
#include <simulationdata.h>

// --------------------------------------------------------------------------------
// --- mandatory lines of code for mypluginmethod to work within ASPIRE
// --- do not change

extern "C" void call_constructor(PluginMethod **pluginMethod, SimulationData *simdata)
{
  *pluginMethod = new MyMethod(simdata);
}
extern "C" void call_destructor(PluginMethod *pluginMethod) { delete (MyMethod *)pluginMethod; }

// --------------------------------------------------------------------------------

MyMethod::MyMethod(SimulationData *_simulationData) : PluginMethod(_simulationData)
{
}

MyMethod::~MyMethod()
{
}

void MyMethod::init()
{
  std::string fname = "cpp/getting_started/tests/bomex/some_dataset.nc";

  // the member dataset definied in mymethod.h which can be accessed in other methods as well
  m_ds = std::make_shared<Dataset>();

  NcReader ncreader_member = NcReader(m_ds.get(), fname);

  ncreader_member.read_variable("some_variable", ASP::Type::DOUBLE);

  // make sure to get variable by reference, we do not want to copy
  ASP::Variable &var = m_ds->get_variable("some_variable");

  // print size
  std::cout << var.get_outer_size() << std::endl;

  // access raw pointer and one element in the array
  std::cout << var.get_cpu_ptr<double>()[10] << std::endl;

  // dataset that only exists in current scope
  Dataset ds = Dataset();

  NcReader ncreader_scope = NcReader(&ds, fname);

  ncreader_scope.read_variable("time", ASP::Type::DOUBLE);

  // make sure to get variable by reference, we do not want to copy
  ASP::Variable &time = ds.get_variable("time");

  // access raw pointer and one element in the array
  std::cout << time.get_cpu_ptr<double>()[1] << std::endl;

  // xtensor is available in all aspkit images
  xt::xarray<double> arr1
      {{1.0, 2.0, 3.0},
       {2.0, 5.0, 7.0},
       {2.0, 5.0, 7.0}};

    xt::xarray<double> arr2
      {5.0, 6.0, 7.0};

    xt::xarray<double> res = xt::view(arr1, 1) + arr2;

    std::cout << res << std::endl;
}

void MyMethod::doit()
{
  // access the member dataset read in init
  ASP::Variable& var = m_ds->get_variable("some_variable");
  std::cout << var.get_cpu_ptr<double>()[8] << std::endl;
}
