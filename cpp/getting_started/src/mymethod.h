#ifndef MYMETHOD_H
#define MYMETHOD_H

#include <memory>

#include <pluginmethod.h>

class SimulationData;
class Dataset;

class MyMethod : public PluginMethod
{
public:
  MyMethod(SimulationData *);
  virtual ~MyMethod();

  virtual void init() override;
  virtual void doit() override;

private:
  std::shared_ptr<Dataset> m_ds;
};

#endif
