&RUN
    lstrict_parser = .true.
    runtime = 21600
    ladaptive = .true.
    dtmax = 10.0
    courant = 1.0
    peclet = 0.5
    irandom = 43
    randthl = 0.1
    randqt = 2.5e-05
    nsv = 1
/

&DOMAIN
    Nx = 64
    Ny = 64
    Nz = 75
    Lx = 6400.0
    Ly = 6400.0
    dzin = 40
    dzgrowth = 0.0
    xlat = 13.0
    xlon = 59.0
/

&LOCATION
    reference = 'CCB'
    position = 0, 0, 0
/

&INPUTPROF
    lactive = .true.
    vg = 0, 0, 0, 0, 0, 0
    ug = -10, -9.46, -9.1, -7.3, -6.4, -4.6
    wfls = 0, -0.0013, -0.0022, -0.0065, 0, 0
    v = 0, 0, 0, 0, 0, 0
    u = -9.1, -9.1, -9.1, -7.3, -6.4, -4.6
    thl = 298.7, 298.7, 298.7, 302.5, 308.0, 312.5
    qt = 0.017, 0.0166, 0.0163, 0.0107, 0.0042, 0.0027
    z = 0.0, 300, 500.0, 1500.0, 2000.0, 3000.0
    sv0 = 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
    dTdtls = -2.315e-05, -2.315e-05, -2.315e-05, -2.315e-05, -1.54321e-05, 0
    dqtdtls = -1.2e-8, -1.2e-8, 0, 0, 0, 0 
/

&PHYSICS
    lmoist = .true.
    lcoriol = .true.
/

&LBC
    ilbc = 300
    ps = 101500.0
    z0m = 0.0002
    z0h = 0.0002
    ustar = 0.28
    wqts = 5.2e-05
    wsvs = 1.0
    wThls = 0.008
/

&DYNAMICS
    ibas_prf = 2
    cu = -8.0
    cv = 0.0
    iadv_mom = 2
    iadv_thl = 5
    iadv_qt = 5
/

&STATPROF
    lactive = .true.
    dtav = 60
    dtwrite = 60
/

&STATSLICES
    lactive = .true.
    dtav = 60
    dtwrite = 60
    var = 'M,LWP'
    i = 38
    j = 47
    k = 0, 8, 9
/

&SGSALT
    scheme = 'sullivan'
    cs = 0.25
    stabCor = 2
/

&VIEW
    lactive = .true.
    lookat = 0.5, 0.5, 0.5
    lookfrom = 0.4, -0.8, 2.0
    vup = 0, 0, 1
    fov = 50.0
    var = 'ql'
    satval = 0.1
    satcolor = 1, 1, 1
    pixwidth = 640
    pixheight = 480
    lwrite = .false.
/

&STATSIMDATA
    lactive = .true.
    var = 'Thl[5,ave(:),ave(:)],qt[5::5,ave(:),ave(:)],u[2::2,ave(16:48),ave(16:48)],ilbc_int[ave(16:48),ave(32:64)]'
    dtav = 60
    dtwrite = 60
/

&ZCROSS
    lactive = .true.
    var = 'u'
    k = 0
    colormap = 'inferno'
/

&YCROSS
    lactive = .true.
    var = 'u'
    j = 32
    colormap = 'inferno'
/

&XCROSS
    lactive = .true.
    var = 'u'
    i = 32
    colormap = 'inferno'
/
