#include "mymethod.h"

#include <iostream>

#include <simulationdata.h>
#include <environment.h>

// --------------------------------------------------------------------------------
// --- mandatory lines of code for mypluginmethod to work within ASPIRE
// --- do not change

extern "C" void call_constructor(PluginMethod** pluginMethod, SimulationData *simdata) {
    *pluginMethod = new MyMethod(simdata);
}
extern "C" void call_destructor(PluginMethod* pluginMethod) { delete (MyMethod*)pluginMethod; }

// --------------------------------------------------------------------------------

MyMethod::MyMethod(SimulationData *_simulationData) : PluginMethod(_simulationData)
{

}

MyMethod::~MyMethod()
{
}

void MyMethod::init()
{
}

void MyMethod::doit()
{
  float tau = 10.;

  int nx = simdata->get_scalar("nx").as<int>();
  int ny = simdata->get_scalar("ny").as<int>();
  int nz1 = simdata->get_scalar("nz").as<int>() + 1;

  ASP::Variable& u = simdata->get_variable("u");
  ASP::Variable& up = simdata->get_variable("up");

  ASP::Variable& v = simdata->get_variable("v");
  ASP::Variable& vp = simdata->get_variable("vp");

  u.gpu2cpu();
  up.gpu2cpu();

  v.gpu2cpu();
  vp.gpu2cpu();

  float* pu = u.get_cpu_ptr<float>();
  float* pup = up.get_cpu_ptr<float>();

  float* pv = v.get_cpu_ptr<float>();
  float* pvp = vp.get_cpu_ptr<float>();

  // enact a total of ngx*ngy small linear drag obstacles on center of the domain handled by each gpu
  int size_small = 4;
  for (int k=0; k < nz1; k++) {
    for (int j=0; j < ny; j++) {
      for (int i=0; i < nx; i++) {
        int n = i + nx*(j + ny*k);

        if (i > nx/2-size_small && i < nx/2+size_small && j > ny/2-size_small && j < ny/2+size_small && k > nz1/2-size_small && k < nz1/2+size_small) {
          pup[n] += -1./tau * pu[n];
          pvp[n] += -1./tau * pv[n];
        }

      }
    }
  }

  if (Environment::ngx > 1 || Environment::ngy > 1) {
    // enact a single, large linear drag obstacle on center of the complete domain 
    int size_large = 8;
    int NX = nx * Environment::ngx;
    int NY = ny * Environment::ngy;
    for (int k=0; k < nz1; k++) {
      for (int j=0; j < ny; j++) {
        for (int i=0; i < nx; i++) {
          int n = i + nx*(j + ny*k);

          int I = i + Environment::gx * nx;
          int J = j + Environment::gy * ny;

          if (I > NX/2-size_large && I < NX/2+size_large && J > NY/2-size_large && J < NY/2+size_large && k > nz1/2-size_large && k < nz1/2+size_large) {
            pup[n] += -1./tau * pu[n];
            pvp[n] += -1./tau * pv[n];
          }

        }
      }
    }
  }

  up.cpu2gpu();
  vp.cpu2gpu();
}
