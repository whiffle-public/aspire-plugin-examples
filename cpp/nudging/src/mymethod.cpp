#include "mymethod.h"

#include <iostream>

#include <ncwriter.h>
#include <simulationdata.h>
#include <nmlreader.h>
#include <helper.h>
#include <dtf.h>

// --------------------------------------------------------------------------------
// --- mandatory lines of code for mypluginmethod to work within ASPIRE
// --- do not change

extern "C" void call_constructor(PluginMethod** pluginMethod, SimulationData *simdata) {
    *pluginMethod = new MyMethod(simdata);
}
extern "C" void call_destructor(PluginMethod* pluginMethod) { delete (MyMethod*)pluginMethod; }

// --------------------------------------------------------------------------------

MyMethod::MyMethod(SimulationData *_simulationData) : PluginMethod(_simulationData)
{

}

MyMethod::~MyMethod()
{
}

void MyMethod::init()
{
  ASP::Variable& var_uwish = simdata->add_variable("uwish","m/s","target profile u",simdata->get_grid("zf"),ASP::Type::FLOAT);  
  ASP::Variable& var_vwish = simdata->add_variable("vwish","m/s","target profile v",simdata->get_grid("zf"),ASP::Type::FLOAT);  
	  
  float* uwish = var_uwish.get_cpu_ptr<float>();
  float* vwish = var_vwish.get_cpu_ptr<float>();

  int nz = simdata->get_scalar("nz").as<int>();

  for (int k=0; k <= nz; k++) {
	  uwish[k] = 1.0;
	  vwish[k] = 0.0;
  }

}

void MyMethod::doit()
{
  
  // std::cerr << "do it\n";

  float tau = 600.;

  int nx = simdata->get_scalar("nx").as<int>();
  int ny = simdata->get_scalar("ny").as<int>();
  int nz = simdata->get_scalar("nz").as<int>();


  ASP::Variable& var_up = simdata->get_variable("up");
  ASP::Variable& var_vp = simdata->get_variable("vp");

  float* up = var_up.get_cpu_ptr<float>();
  float* vp = var_vp.get_cpu_ptr<float>();


  ASP::Variable& var_uav = simdata->get_variable("u_av");
  ASP::Variable& var_vav = simdata->get_variable("v_av");

  float* uav = var_uav.get_cpu_ptr<float>();
  float* vav = var_vav.get_cpu_ptr<float>();

  var_up.gpu2cpu();
  var_vp.gpu2cpu();


  ASP::Variable& var_uwish = simdata->get_variable("uwish");
  ASP::Variable& var_vwish = simdata->get_variable("vwish");

  float* uwish = var_uwish.get_cpu_ptr<float>();
  float* vwish = var_vwish.get_cpu_ptr<float>();


  for (int k=0; k <= nz; k++) {
    for (int j=0; j < ny; j++) {
      for (int i=0; i < nx; i++) {
        int l = i + nx*(j + ny*k);

        up[l] -= (uav[k] - uwish[k])/tau;
        vp[l] -= (vav[k] - vwish[k])/tau;

      }
    }
  }

  var_up.cpu2gpu();
  var_vp.cpu2gpu();
}
