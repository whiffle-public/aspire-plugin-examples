#ifndef MYMETHOD_H
#define MYMETHOD_H

#include <pluginmethod.h>

class SimulationData;

class MyMethod : public PluginMethod {
public:
  MyMethod(SimulationData*);
  virtual ~MyMethod();

  virtual void init() override;
  virtual void doit() override;

};

#endif
