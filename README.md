# ASPIRE Plugin Examples

## Building and testing

For building ASPIRE plugins the ``aspkit-devel`` image is required. Assuming Singularity (see documentation for more information):

```
singularity pull --docker-login docker://registry.whffl.nl/aspkit-license/aspkit-devel:8.2.3
```

```
ln -rs aspkit-devel_8.2.3.sif aspkit-devel_latest.sif
singularity shell --nv aspkit-devel_latest.sif
cd cpp/linear_drag_obstacle
mkdir build
cd build
cmake .. -DCPP_PLUGIN_EXAMPLE_MASTER_PROJECT=ON
make -j
export ASPIRE_CPP_LINEAR_DRAG_PLUGIN_EXAMPLE_LIB=$(pwd)/src
export ASPIRE_LICENSE_KEY=<your licence key>
cd ../tests/bomex
aspire graspIn.000.nml
```

## Interactive debugging with VS Code in Singularity image

On your local machine, add to your `~/.ssh/config`:

```
Host aspkit-devel~<your machine>
  User <your username>
  HostName <your machine>
  RemoteCommand singularity shell --nv <path-to-image>/aspkit-devel_latest.sif
  RequestTTY yes
```

In your VS Code settings add:

```
    "remote.SSH.enableRemoteCommand": true,
    "remote.SSH.serverInstallPath": {
        "aspkit-devel~<your machine>": "~/.vscode-container/aspkit",
	}
```

And connect to remote host `aspkit-devel~<your machine>` with VS Code. 

Make sure to install the required VS Code extenstions, see `.vscode/extensions.json`.