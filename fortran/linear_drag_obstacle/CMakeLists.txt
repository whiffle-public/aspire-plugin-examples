cmake_minimum_required(VERSION 3.8)
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

set(FORTRAN_PLUGIN_EXAMPLE_PROJECT_NAME linear_drag_obstacle_fortran_plugin_example)
# all aspkit submodules have the same version
set(FORTRAN_PLUGIN_EXAMPLE_VERSION ${ASPKIT_VERSION})
set(FORTRAN_PLUGIN_EXAMPLE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})

# language C is required by older cmake for building Fortran-C interfaces
project(${FORTRAN_PLUGIN_EXAMPLE_PROJECT_NAME} VERSION ${FORTRAN_PLUGIN_EXAMPLE_VERSION} LANGUAGES CXX C Fortran)

option(FORTRAN_PLUGIN_EXAMPLE_MASTER_PROJECT "BUILD AS STAND-ALONE PROJECT" OFF)

# checks recommended by https://enccs.github.io/cmake-workshop/cxx-fortran/ for mixing c and fortran
include(FortranCInterface)
FortranCInterface_VERIFY(CXX)

option(INCLUDE_ASPIRE_PLUGIN_SOURCE "INCLUDE ASPIRE PLUGIN EXAMPLE SOURCE CODE IN INSTALL" OFF)

# Source directories
add_subdirectory(src)
