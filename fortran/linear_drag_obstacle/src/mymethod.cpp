#include "mymethod.h"

#include <iostream>

#include <ncwriter.h>
#include <simulationdata.h>
#include <nmlreader.h>
#include <helper.h>
#include <dtf.h>

// --------------------------------------------------------------------------------
// --- mandatory lines of code for mypluginmethod to work within ASPIRE
// --- do not change

extern "C" void call_constructor(PluginMethod** pluginMethod, SimulationData *simdata) {
    *pluginMethod = new MyMethod(simdata);
}
extern "C" void call_destructor(PluginMethod* pluginMethod) { delete (MyMethod*)pluginMethod; }

// --------------------------------------------------------------------------------

MyMethod::MyMethod(SimulationData *_simulationData) : PluginMethod(_simulationData)
{

}

MyMethod::~MyMethod()
{
}

void MyMethod::init()
{
}

void MyMethod::doit()
{
  float tau = 10.;

  int nx = simdata->get_scalar("nx").as<int>();
  int ny = simdata->get_scalar("ny").as<int>();
  int nz1 = simdata->get_scalar("nz").as<int>() + 1;

  ASP::Variable& u = simdata->get_variable("u");
  ASP::Variable& up = simdata->get_variable("up");

  ASP::Variable& v = simdata->get_variable("v");
  ASP::Variable& vp = simdata->get_variable("vp");

  u.gpu2cpu();
  up.gpu2cpu();

  v.gpu2cpu();
  vp.gpu2cpu();

  float* pu = u.get_cpu_ptr<float>();
  float* pup = up.get_cpu_ptr<float>();

  float* pv = v.get_cpu_ptr<float>();
  float* pvp = vp.get_cpu_ptr<float>();

  doit_fortran(&nx, &ny, &nz1, &tau, pu, pup);
  doit_fortran(&nx, &ny, &nz1, &tau, pv, pvp);

  up.cpu2gpu();
  vp.cpu2gpu();
}
