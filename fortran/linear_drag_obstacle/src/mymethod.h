#ifndef MYMETHOD_H
#define MYMETHOD_H

#include <pluginmethod.h>

class SimulationData;

class MyMethod : public PluginMethod {
public:
  MyMethod(SimulationData*);
  virtual ~MyMethod();

  virtual void init() override;
  virtual void doit() override;

};

extern"C" {
  void doit_fortran(
    int* nx, int* ny, int* nz, float* tau, float* fld,
    float* fldp
    );
}

#endif
